# FINESSE 2: Getting started


## About
[FINESSE](http://www.gwoptics.org/finesse/) (Frequency domain INterfErometer Simulation SoftwarE) is a 
numeric simulation for laser interferometers using the frequency domain and Hermite-Gauss modes. 
It is open source software distributed for OSX, Linux, and Windows.

[PYKAT](http://www.gwoptics.org/pykat/) is a Python wrapper for FINESSE. 
It enables a user to easily perform complex sets of modelling tasks efficiently, enables FINESSE to be used in conjunction with the wide variety of Python packages, and makes it easy to use FINESSE from Jupyter notebooks.
This has become the preferred method of using FINESSE now for many users. 
We support PYKAT for Python 3.x. 

This [Binder](https://mybinder.org/) (linked from [www.gwoptics.org/finesse/binder/](www.gwoptics.org/finesse/binder/)) provides a [Jupyter](https://jupyter.org/) environment with both FINESSE and PYKAT pre-installed, so that new users can 'try before they buy'. 
The initial loading time may be slow depending usage and server load, but then you can play around with the live code in the example notebook, create your own notebook, or upload another notebook you wish to test-run.
Note that work *cannot* be saved long-term in this environment, and the Binder will shut down after 10mins of inactivity (e.g. closing the browser tab), so it is not suitable as a substitute for installation.



## Get FINESSE and PYKAT
If you decide to continue using FINESSE, we now strongly recommend installation via the `conda` package manager from Anaconda. 
Please follow the instructions provided in the [FINESSE installation guide](http://www.gwoptics.org/finesse/download/Install.html).


## Useful Resources
The [gwoptics.org](http://www.gwoptics.org) website is the primary resource for FINESSE users, including:

* [FINESSE syntax reference](http://www.gwoptics.org/finesse/reference/)
* [FINESSE cheatsheet / FAQ](http://www.gwoptics.org/finesse/reference/cheatsheet.php)
* [FINESSE manual (pdf)](http://www.gwoptics.org/finesse/download/manual.pdf)

Those wishing to learn more about how to use FINESSE/PYKAT should follow the training materials provided through the [Learn Laser Interferometry](http://www.gwoptics.org/learn/) course.

Some examples of FINESSE code-only models (i.e. no PYKAT) are available on the [FINESSE website](http://www.gwoptics.org/finesse/#examples). Further simple PYKAT examples are available from the [PYKAT GitLab page](https://git.ligo.org/finesse/pykat/tree/master/examples). 

The review article [Interferometer Techniques for Gravitational-Wave Detection](https://link.springer.com/article/10.1007/s41114-016-0002-8) is written by some of the team behind FINESSE and includes FINESSE examples for many of the key topics covered. It is therefore a good resource for those wanting to understand more about interferometry as applied to gravitational-wave detection, and as implemented in FINESSE.

## Contact

GW community members ([LIGO](https://www.ligo.org/), [Virgo](http://www.virgo-gw.eu/), [KAGRA](https://gwcenter.icrr.u-tokyo.ac.jp/en/)) can access the FINESSE chat channel on [chat.ligo.org](https://chat.ligo.org/ligo/channels/finesse) using their albert.einstein login. 

The [Ifosim logbooks](https://logbooks.ifosim.org/) provide a forum for users to share more examples and references of interferometry simulations, mostly generated in the course of gravitational-wave research. Posts are public; write-access is available for all GW community members via albert.einstein, and to others on request. <!-- link 'on request' somewhere? -->

General support is available by emailing us at finesse-support(at)nikhef.nl. If you have trouble with installation or think you have encountered a bug, please include the following information in your message:

 * Operating System and installation method used
 * versions of FINESSE and PYKAT you are using 
 * [Minimum Working Example](https://en.wikipedia.org/wiki/Minimal_working_example) to reproduce the bug you encountered







