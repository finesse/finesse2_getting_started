{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#342184; padding:6px; color:#f3f0ff;\">\n",
    "\n",
    "# Try Finesse 2 & PyKat\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this interactive notebook we introduce some key aspects of  <a href=http://www.gwoptics.org/finesse/>Finesse</a> and <a href =http://www.gwoptics.org/pykat/> PyKat</a>.\n",
    "You can play around with the live code presented here, create your own notebook (`File > New Notebook`), or upload a notebook you wish to test-run. Note that **changes are not saved after exiting the Binder session**, so you should download a local copy of any notebooks you wish to retain.\n",
    "\n",
    "### In this notebook\n",
    "\n",
    "Finesse numerically models optical systems in the _frequency domain_ using <a href = https://en.wikipedia.org/wiki/Plane_wave>plane waves</a> or <a href=https://en.wikipedia.org/wiki/Gaussian_beam>Gaussian beams</a>, including higher-order modes.\n",
    "\n",
    "This notebook demonstrates a common task: modeling a 2-mirror optical cavity. It is based on material from the <a href=http://www.gwoptics.org/learn/index.html>Learn Laser Interferometry</a> course on gwoptics.org. We will:\n",
    "1. Construct a plane-waves model of the cavity, plotting the power transmitted, reflected, and circulating from the cavity as its length is changed. \n",
    "2. Make the model more realistic using Gaussian beams and curved optics, then re-produce the powers plot to check that the behaviour still makes sense.\n",
    "3. Plot the profile of the beam transmitted from the cavity.\n",
    "4. Show that the cavity can be used as a mode cleaner.\n",
    "\n",
    "You can learn more about 2-mirror cavities, also known as Fabry-Perot  cavities or interferometers, by reading <a href=https://link.springer.com/article/10.1007/s41114-016-0002-8#Sec33>section 5.1</a> of Interferometer Techniques for Gravitational-Wave Detection and <a href=http://www.gwoptics.org/learn/02_Plane_waves/01_Fabry_Perot_cavity/index.html>section 2</a> of the Learn Laser Interferometry course."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Getting Started\n",
    "\n",
    "You may wish to first read through <a href=\"http://www.gwoptics.org/learn/01_Introduction/01_Getting_Started/index.html\">Getting Started</a> for a general introduction to Jupyter notebooks, Finesse and Pykat. \n",
    "\n",
    "Typical packages needed in simple Finesse/PyKat simulations:\n",
    "\n",
    "(type shift+enter to run)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pykat import finesse        # import the whole pykat.finesse package\n",
    "from pykat.commands import *     # import all packages in pykat.commands\n",
    "import numpy as np               # for basic math/sci/array functions\n",
    "import matplotlib.pyplot as plt  # for plotting\n",
    "\n",
    "# tell the notebook to automatically show plots inline below each cell\n",
    "%matplotlib inline               \n",
    "# use pykat's plotting style. change dpi to change plot sizes on your screen\n",
    "pykat.init_pykat_plotting(dpi=90)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Optical Layout\n",
    "\n",
    "The setup we want to model is shown in the figure below. A *cavity*, or *optical resonator* is formed by the two partially-transparent mirrors M1 and M2. To measure the reflected, transmitted, and circulating power in the cavity, we add 3 photodiodes (these do not affect the beam propagation).\n",
    "\n",
    "<img src=http://www.gwoptics.org/learn/02_Plane_waves/01_Fabry_Perot_cavity/fabry-perot.svg>\n",
    "\n",
    "\n",
    "In our case we have:\n",
    "\n",
    "```\n",
    "                       M1                     M2\n",
    "laser (n0) ------> (n1)|(n2) <----------> (n3)|(n4)\n",
    "```\n",
    "\n",
    "and will add photodiodes looking at the beams at nodes `n1` (refl), `n3` (circ) and `n4` (trans)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plane-Waves Finesse Model\n",
    "\n",
    "### base model\n",
    "The code below builds a plane-waves model with:\n",
    " * 1 W laser with 0 offset to the default wavelength (This is 1064nm in Finesse, unless you tell it otherwise)\n",
    " * a short space from laser to M1\n",
    " * mirrors 'M1' and 'M2' with T=0.15 and zero loss\n",
    " * a cavity length of 1 meter.\n",
    " \n",
    "You may find the <a href=http://www.gwoptics.org/finesse/reference/>Finesse syntax reference</a> helpful for interpreting `basecode`, and the <a href=http://www.gwoptics.org/finesse/reference/cheatsheet.php>Cheatsheet</a> useful for understanding definitional conventions, e.g. lengths vs 'tunings' (`phi`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Finesse code for the optical layout\n",
    "basecode = \"\"\"\n",
    "l laser 1 0 n0\n",
    "s s0 0.1 n0 n1\n",
    "\n",
    "#the cavity:\n",
    "m1 M1 0.15 0 0 n1 n2\n",
    "s scav 1 n2 n3\n",
    "m1 M2 0.15 0 0 n3 n4\n",
    "\"\"\"\n",
    "\n",
    "#initialise Finesse with a new empty kat object\n",
    "basekat = finesse.kat() \n",
    "#tell Finesse to talk less\n",
    "basekat.verbose = False\n",
    "#parse the Finesse code into PyKat\n",
    "basekat.parse(basecode)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### plotting powers\n",
    "Here we make a copy of the `basekat` object, add the three detectors as noted in the sketches above, then add an `xaxis` command to vary the tuning of M2 over a range of one wavelength. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#create an independent copy of basekat\n",
    "kat1 = basekat.deepcopy()\n",
    "#write the code for the PDs and xaxis\n",
    "PDcode = \"\"\"\n",
    "# Photo diodes measuring DC-power\n",
    "pd refl n1          # Reflected field\n",
    "pd circ n3          # Circulating field\n",
    "pd tran n4          # Transmitted field\n",
    "\n",
    "## Simulation instructions ##\n",
    "xaxis M2 phi lin -20 200 300    # Varying tuning of input mirror m2.\n",
    "yaxis abs                       # Plotting the amplitude of the detector measurements. \n",
    "\"\"\"\n",
    "#parse the code for the new PDs and xaxis to the copied kat object \n",
    "kat1.parse(PDcode)\n",
    "#run the simulation, and store the result in out1\n",
    "out1 = kat1.run()\n",
    "#plot the output of all detectors in kat1\n",
    "out1.plot(xlabel='Position of mirror M2 [deg]',\n",
    "          ylabel='Power [W]', \n",
    "          title = 'Power vs. microscopic cavity length change')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Gaussian Finesse Model\n",
    "    \n",
    "###  instructing Finesse to use Gaussian beams, add curvatures to the cavity mirrors, and reproduce the tuning plot for the upgraded model\n",
    "\n",
    "Below we make a new copy of `basekat` and parse additional code to:\n",
    " - add curvatures of $Rc_1$=-0.6m, $Rc_2$=0.6m to mirrors M1 and M2 respectively \n",
    " - calculate the coupling of fields considering the HG00 mode only\n",
    " - define the cavity formed by M1 and M2\n",
    " - plot the powers again to check the the cavity still behaves as we expect.\n",
    "\n",
    "See the <a href=http://www.gwoptics.org/finesse/reference/cheatsheet.php>Cheatsheet</a> for the RoC sign convention and overview of the `cav` command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#make a copy of the base\n",
    "gausskat = basekat.deepcopy()\n",
    "#parse the required commands\n",
    "gausskat.parse(\"\"\"\n",
    "attr M1 Rc -0.6\n",
    "attr M2 Rc 0.6\n",
    "cav FPcav M1 n2 M2 n3\n",
    "maxtem 0\n",
    "\"\"\")\n",
    "#make a copy we can play with\n",
    "kat2 = gausskat.deepcopy()\n",
    "#parse the code for the PDs again\n",
    "kat2.parse(PDcode)\n",
    "#run the code\n",
    "out2 =kat2.run()\n",
    "#plot the result\n",
    "out2.plot(xlabel='Position of mirror M2 [deg]',\n",
    "          ylabel='Power [W]', \n",
    "          title = 'Power vs. microscopic cavity length change (Gaussian version)')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###  transmitted beam profile"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#pykat commands for the beam profiler\n",
    "kat3 = gausskat.deepcopy()\n",
    "kat3.parse(\"\"\"\n",
    "beam BP n4\n",
    "xaxis BP x lin -4 4 30\n",
    "x2axis BP y lin -4 4 30\"\"\")\n",
    "out3=kat3.run()\n",
    "\n",
    "#making the plot\n",
    "plt.figure(figsize=(6,6)) #making sure the image is square\n",
    "plt.pcolormesh(out3.x, out3.y,out3[\"BP\"], shading='auto')\n",
    "plt.xlabel(\"x position [# spotsizes]\")\n",
    "plt.ylabel(\"y position [# spotsizes]\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "  \n",
    "### Mode cleaning\n",
    "\n",
    "The code below does the following:\n",
    "1. use the `tem` command to add some higher order mode content to the input beam.\n",
    "2. Plots the resulting beam profile at the input node. \n",
    "3. Plots the cavity powers with tuning.\n",
    "4. Plots the beam profile transmitted from the cavity at several different tunings of interest, to confirm that that cavity 'cleans' the beam."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# add some HOMs \n",
    "MCkat = gausskat.deepcopy()\n",
    "MCkat.parse(\"\"\"\n",
    "tem laser 1 0 0.1 0\n",
    "maxtem 3\n",
    "\"\"\")\n",
    "\n",
    "# input beam profile\n",
    "mckat1 = MCkat.deepcopy()\n",
    "mckat1.parse(\"\"\"\n",
    "beam BP n0\n",
    "xaxis BP x lin -4 4 30\n",
    "x2axis BP y lin -4 4 30\"\"\")\n",
    "mcout1 = mckat1.run()\n",
    "#plot\n",
    "plt.figure(figsize=(6,6)) #making sure the image is square\n",
    "plt.pcolormesh(mcout1.x, mcout1.y,mcout1[\"BP\"])\n",
    "plt.xlabel(\"x position [# spotsizes]\")\n",
    "plt.ylabel(\"y position [# spotsizes]\")\n",
    "plt.title(\"input beam profile\")\n",
    "\n",
    "#cavity tuning vs powers\n",
    "mckat2 = MCkat.deepcopy()\n",
    "mckat2.parse(PDcode)\n",
    "mcout2 = mckat2.run()\n",
    "#plot\n",
    "plt.figure()\n",
    "mcout2.plot(xlabel='Position of mirror M2 [deg]',\n",
    "          ylabel='Power [W]', \n",
    "          title = 'Power vs. microscopic cavity length change (with HOMs)')\n",
    "\n",
    "#transmitted profiles at interesting tunings\n",
    "#no detuning\n",
    "mckat3 = MCkat.deepcopy()\n",
    "mckat3.parse(\"\"\"\n",
    "beam BP n4\n",
    "xaxis BP x lin -4 4 30\n",
    "x2axis BP y lin -4 4 30\"\"\")\n",
    "mcout3 = mckat3.run()\n",
    "#70deg detuning\n",
    "mckat4 = mckat3.deepcopy()\n",
    "mckat4.M2.phi = 70\n",
    "mcout4 = mckat4.run()\n",
    "#plots\n",
    "plt.figure(figsize=(12,6))\n",
    "plt.subplot(1,2,1)\n",
    "plt.pcolormesh(mcout3.x, mcout3.y,mcout3[\"BP\"], shading='auto')\n",
    "plt.xlabel(\"x position [# spotsizes]\")\n",
    "plt.ylabel(\"y position [# spotsizes]\")\n",
    "plt.title(\"output beam profile, 0deg\")\n",
    "plt.subplot(1,2,2)\n",
    "plt.pcolormesh(mcout4.x, mcout4.y,mcout4[\"BP\"], shading='auto')\n",
    "plt.xlabel(\"x position [# spotsizes]\")\n",
    "plt.ylabel(\"y position [# spotsizes]\")\n",
    "plt.title(\"output beam profile, 70deg\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### A note on pykat and finesse commands:\n",
    "\n",
    "Here, we have largely used pykat to just parse code written in *Finesse* syntax (`kat.parse(\"<FINESSE_SYNTAX>\")`) and to plot the outputs.\n",
    "\n",
    "Many commands also have a direct equivalent in pykat, and pykat has many additional features which become more useful once the base kat object has been created. Some examples:\n",
    "\n",
    " - `kat.M1.phi = 10` --sets the tuning of M1 to 10$^\\circ$\n",
    " - `kat.M2.setRTL(1,0,0)` --sets the power Reflectivity, Transmissivity, and Loss of M2 to 1, 0, and 0 respectively\n",
    " - `kat.noxaxis = True` --tells the code to ignore any xaxis command and just compute the detector outputs for the current parameters\n",
    " - `kat.maxtem = 6` --changes the number of HOMs included in the model to include all HG modes up to 6th order."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.5-final"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}